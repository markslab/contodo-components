
export default function LinkList ( props, ref ) {
	const {
		links,
    linkStyle,
    className,
    ...additionalProps
	} = props;
	
	const linkDisplay = links.map( (link, index) => {
	  if ( link.url.length > 0 ) {
	    return (
	      <li>
	      	<a key={ index } href={ link.url }>
	      		{ link.title }
	      	</a> 
	      </li>
	    );
	  }
	} )

	if (linkStyle == 'ol') {
		return (
	  	<div className={ className }>	
	  		<ol>
	  			{ linkDisplay }
	  		</ol>
	  	</div>
		)
	}

	return (
		<div className={ className }>
			<ul>
				{ linkDisplay }
			</ul>
		</div>
	)
}
