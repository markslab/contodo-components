import {
  InnerBlocks,
  MediaUpload,
} from '@wordpress/block-editor';

import { TwoUp } from './two-up';

export const save = ( props ) => {
	const {
		className,
		attributes: {
      subtitle,
      title,
      linkText,
      url,
      absLink,
      content,
      imageWidth,
		},
	} = props;

	return (
		<TwoUp 
			className={ className }
			subtitle={ subtitle }
			title={ title }
			linkText={ linkText }
			url={ url }
			absLink={ absLink }
			content={ content }
			imageWidth={ imageWidth }
		>
			<InnerBlocks.Content />
		</TwoUp>
	);
}
