
import { __ } from '@wordpress/i18n';
import {
	registerBlockType,
} from '@wordpress/blocks';

import { edit } from './edit'; 
import { save } from './save';

registerBlockType( 'contodo-components/project-card', {
	title: __( 'Project Card', 'contodo-components' ),
	icon: 'index-card',
	category: 'contodo-components',
	attributes: {
		title: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		url: {
			type: 'array',
			source: 'children',
			selector: 'a.image',
			attribute: 'href',
		},
		absLink: {
			type: 'string',
		},
		content: {
			type: 'array',
			source: 'children',
			selector: '.description',
		},
	},
	edit: edit,
	save: save 
});
