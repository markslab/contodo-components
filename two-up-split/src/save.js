import {
  InnerBlocks,
} from '@wordpress/block-editor';

import { TwoUpSplit } from './two-up-split';

export const save = ( props ) => {
	const {
		className,
		attributes: {
      anchor,
      background,
      text,
      alignment,
      title,
      subtitle,
      content,
      cta,
      links,
      imageWidth,
		},
	} = props;

	return (
		<TwoUpSplit 
			anchor={ anchor }
			className={ className }
			subtitle={ subtitle }
			title={ title }
			content={ content }
			cta={ cta }
			links={ links }
			background={ background }
			text={ text }
			imageWidth={ imageWidth }
			alignment={ alignment }
		>
			<InnerBlocks.Content />
		</TwoUpSplit>
	);
}
