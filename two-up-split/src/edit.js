import classnames from 'classnames';

import { __ } from '@wordpress/i18n';

import {
  InnerBlocks,
  InspectorControls,
  RichText,
  URLInputButton,
} from '@wordpress/block-editor';

import { 
  Button,
  ColorPicker,
  Panel,
  PanelBody,
  PanelRow,
  SelectControl,
} from '@wordpress/components';

import { LinksList, LinksInput } from '../../_library/links';

export const edit = (props) => {
  const {
      className,
      attributes: {
        background,
        text,
        alignment,
        title,
        subtitle,
        content,
        cta,
        links,
        imageWidth,
        allowedBlocks,
        TEMPLATE,
      },
      setAttributes,
    } = props;

    return (
      <>
        <InspectorControls key={ props.clientId }>
          <Panel header={ __( 'Two Up Split settings', 'contodo-components' ) }>
            <PanelBody
                title={ __( 'Image Width', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <SelectControl
                  label="Width in percentage"
                  value={ imageWidth }
                  className="d-block w-100 mb-2"
                  options={ [
                    { label: '25%', value: 25 },
                    { label: '33%', value: 33 },
                    { label: '42%', value: 42 },
                    { label: '50%', value: 50 },
                    { label: '58%', value: 58 },
                    { label: '67%', value: 67 },
                    { label: '75%', value: 75 },
                  ] }
                  onChange={ ( imageWidth ) => { setAttributes( { imageWidth: imageWidth } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Image Alignment', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <SelectControl
                  label="Image alignment"
                  value={ alignment }
                  className="d-block w-100 mb-2"
                  options={ [
                    { label: 'left', value: 'left' },
                    { label: 'right', value: 'right' },
                  ] }
                  onChange={ ( alignment ) => { setAttributes( { alignment: alignment } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Background Color', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: background} }
                  onChange={ (color) => { setAttributes( { background: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Text Color', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <ColorPicker
                  color={ {hex: text} }
                  onChange={ (color) => { setAttributes( { text: color.hex } ) } }
                  />
              </PanelRow>
            </PanelBody>
            <PanelBody
                title={ __( 'Links', 'contodo-components' ) }
                initialOpen={false}
            >
              <PanelRow>
                <LinksInput
                  links={ links }
                  setAttributes={ setAttributes }
                  />
              </PanelRow>
            </PanelBody>
          </Panel>
        </InspectorControls>
        <div 
          className={ ["contodo", "two-up", "two-up-split", "image-"+imageWidth, "image-"+alignment, className].join(" ") }
          style={ {color: text} }
          >
          <div className={ "image" }>
            <InnerBlocks 
              allowedBlocks={ allowedBlocks } 
              template={ TEMPLATE } 
              templateLock={ 'all' } />
          </div>
          <div className={ classnames("content", "content-top") }>
            <div 
              className="bg-tile"
              style={ {background: background} }></div>
            <RichText
              tagName="h2"
              placeholder={ __( 'Title…', 'contodo-components' ) }
              value={ title }
              onChange={  (v) => setAttributes( { title: v } )  }
            />
            <RichText
              tagName="h5"
              multiline={ false }
              placeholder={ __( 'Subtitle…', 'contodo-components' ) }
              value={ subtitle }
              onChange={  (v) => setAttributes( { subtitle: v } )  }
            />
            <RichText
              tagName="div"
              multiline="p"
              className="description"
              placeholder={ __( 'Content…', 'contodo-components' ) }
              value={ content }
              onChange={  (v) => setAttributes( { content: v } )  }
            />
          </div>
          <div className={ classnames("content", "content-bottom") }>
            <div 
              className="bg-tile"
              style={ {background: background} }></div>
            <RichText
              tagName="span"
              className="cta"
              placeholder={ __( 'Call to action…', 'contodo-components' ) }
              value={ cta }
              onChange={  (v) => setAttributes( { cta: v } )  }
            /> 
            <LinksList
              links={ links }
              />
          </div>
        </div>
      </>
    );
  }